<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'git-project2' );

/** MySQL database username */
define( 'DB_USER', 'git-project2' );

/** MySQL database password */
define( 'DB_PASSWORD', 'git-project2' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'd,yuZ`<CQkXNcSoC3[3Q&mZA.A4uXwC2s#?`vb{%s:&RY^4`~b{MfUG3#9G6U5a,' );
define( 'SECURE_AUTH_KEY',  'Pxp-;5jXW-G.bd$.V5UODgm1zw%B8EOv9EbP,[>QiQH4E[AZp}y3O{`T)rb!HBUe' );
define( 'LOGGED_IN_KEY',    '|u?;oto&YGeVw:X1@:qVj&/B?NdoW&1d>i0dT$b#|m-&4U}{9J/Y(29}Tz(?].4|' );
define( 'NONCE_KEY',        '=_j{$u+bFIX*f6l{9#IvOSUe1Bn&C }DENF@,O8oN^>pt,qwQVv4 {YfldtWr]7)' );
define( 'AUTH_SALT',        '8GNV<~O|@u-Z5{gC<m<%X/>7dvzbYM4#0_e)@}(N7(Qq_(1Ld g?L!48kCEedEih' );
define( 'SECURE_AUTH_SALT', '051 cj@<]<{TXhjo*:*Aap[g&E(&+zva>%C0,iU])|cd6Pd,vs>T/F&w}DScKA3%' );
define( 'LOGGED_IN_SALT',   ';j(RneuO,p{q[U?mfDP2aT>5%D? zKU1@<1o!Q.  [(>)6{?}jj{{<Sd2fDrTBm_' );
define( 'NONCE_SALT',       '1t/y5e+E-ZPFYQv VAUuQ%,Ww`6MD(q~JZ$3;1Mc>b`VJ:}w;+xlX3c~bps<`v81' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
